//sample logic to customize engine
//I have changed method and class names
package CalypsoSample;

import com.calp.engine.Engine;
import com.calp.tk.core.JDatetime;
import com.calp.tk.service.DSConnection;

public class CustomTradeEngine extends Engine {

    private String configName;

    public CustomTradeEngine(DSConnection dsCon, String hostName, int port) {
        super(dsCon, hostName, port);
    }

    @Override
    protected void init(EngContxt engContxt) { //classobject name changed
        super.init(engContxt);
        configName = engContxt.getInitParameter("config", null);

    }
    
    public List<String> getNonPersistentClasses() 
    { 
        List<String> nonpersistentclasses = new ArrayList<String>();
        nonpersistentclasses.add(PSEventCustom.class.getName());
        return nonpersistentclasses;
     }

    @Override
    public boolean process(PSEvnt event) {
        try {
            TradeArray v = dsCon.getRemoteTrd().getTrades(trdFilter, new JDatetime());//method name has been changed
            for (int i = 0; i < v.size(); i++) {
                Trade trade = (Trade) v.elementAt(i);
                System.out.println("Trade : " + trade.getId());
            }
        } catch (Exception exc) {
        }
        return true;
    }
}
