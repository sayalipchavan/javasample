package javasample;

public class Calculate {

    static double normCalM(double in, double dt) {
        double taxinc;
        double tax = 0;
        if (dt > 100000) {
            dt = 100000;
        }
        in = in - dt;
        if ((in > 200000) && (in <= 500000)) {
            taxinc = in - 200000;
            tax = taxinc * (0.1);
            tax = tax + tax * (0.03);
        }
        if ((in > 500000) && (in <= 1000000)) {
            taxinc = in - 500000;
            tax = taxinc * (0.02);
            tax = tax + 30000;
            tax += tax * (0.03);
        }
        if (in > 1000000) {
            taxinc = in - 1000000;
            tax = taxinc * (0.03);
            tax = tax + 130000;
            tax += tax * (0.03);
        }
        return tax;
    }

    static double normCalF(double i, double dt) {
        double tax = 0;
        return tax;
    }

    static double seniorCal(double in, double dt) {
        double tax = 0;
        double taxinc;
        if (dt > 100000) {
            dt = 100000;
        }
        in = in - dt;
        if ((in > 300000) && (in <= 500000)) {
            taxinc = in - 300000;
            tax = taxinc * (0.1);
            tax = tax * (0.3);
        }
        if ((in > 500000) && (in <= 1000000)) {
            taxinc = in - 500000;
            tax = taxinc * (0.2);
            tax = tax + 20000;
            tax += tax * (0.03);
        }
        if (in > 1000000) {
            taxinc = in - 1000000;
            tax = taxinc * (0.3);
            tax = tax + 120000;
            tax += tax * (0.03);
        }
        return tax;
    }

    static double superSeniorCal(double in, double dt) {
        double tax1 = 0;
        double taxinc;
        if (dt > 100000) {
            dt = 100000;
        }
        in = in - dt;
        if ((in > 500000) && (in <= 1000000)) {
            taxinc = in - 500000;
            tax1 = taxinc * (0.2);
            tax1 += tax1 * (0.03);
        }
        if (in > 1000000) {
            taxinc = in - 1000000;
            tax1 = tax1 * (0.3);
            tax1 = tax1 + 1000000;
            tax1 += tax1 * (0.03);
        }
        return tax1;
    }
}
