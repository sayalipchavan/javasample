package javasample;

import static org.junit.Assert.assertEquals;
import org.junit.Before;
import org.junit.Test;

public class CalculateTest {

    private static final double DELTA = 1e-15;
    Calculate c;

    @Before
    public void setUp() {
        c = new Calculate();
    }

    public void testNormalCalM() {
        assertEquals(0.0, Calculate.normCalM(100000.0, 20000.0), DELTA);

        assertEquals(20600.0, Calculate.normCalM(500000.0, 10000.0), DELTA);

        assertEquals(161710.0, Calculate.normCalM(2000000.0, 30000.0), DELTA);
        assertEquals(39140.0, Calculate.normCalM(1000000.0, 40000.0), DELTA);
    }

    @Test
    public void testSeniorCal() {
        assertEquals(0.0, Calculate.seniorCal(100000.0, 20000.0), DELTA);
        assertEquals(3000.0, Calculate.seniorCal(500000.0, 100000.0), DELTA);
        assertEquals(401700.0, Calculate.seniorCal(2000000.0, 300000.0), DELTA);
        assertEquals(103000.0, Calculate.seniorCal(1000000.0, 400000.0), DELTA);
    }

    @Test
    public void testSuperSeniorCal() {
        assertEquals(0.0, Calculate.superSeniorCal(100000.0, 20000.0), DELTA);
        assertEquals(20600.0, Calculate.superSeniorCal(700000.0, 100000.0), DELTA);
        assertEquals(1030000.0, Calculate.superSeniorCal(2000000.0, 300000.0), DELTA);
    }
}
