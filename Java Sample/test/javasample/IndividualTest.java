
package javasample;

import mockit.Mocked;
import mockit.NonStrictExpectations;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
public class IndividualTest
{
Individual ind;
private static final double DELTA=1d;
@Before
public void setUp ()
{
ind=new Individual();
}
@Test
public void testGetCategoryForMale ()
{
Assert.assertEquals(39140.0,ind.getCategory (“M”,1000000,200000),DELTA);
}

@Test
public void testGetCategoryForSenior ()
{
new NonStrictExpectations ()
{
@Mocked
Calculate calc;
{
Calculate.seniorCal(1000000,200000);
returns(5000.0);
}
};
Assert.aassertEquals(5000.0,ind.getCategory (“S”,1000000,200000),DELTA);
}

@Test
Public void testGetCategoryForSuperSenior ()
{
ind.setData(“ABC”,80,”SS”,900000,100000);
Assert.assertEquals(61800.0,ind.getCategory (“SS”,900000,100000),DELTA);
}
}
